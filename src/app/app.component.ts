import { Component, OnInit, ViewChild } from '@angular/core';
import {MatSort, MatTableDataSource } from '@angular/material';


export interface TableDaniel{
  nombre:string;
  edad:string,
  ciudad:string;
  propina:string;
  nuevacolumna:string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent  implements OnInit{
  title = 'Tables';
  @ViewChild(MatSort) sort: MatSort;
  data =[
    {
      nombre:'Daniel',
      edad:23,
      ciudad:'Toluca',
      propina:190
    },
    {
      nombre:'Ariel',
      edad:30,
      ciudad:'Metepec',
      propina:12
    },
    {
      nombre:'Raul',
      edad:30,
      ciudad:'CDMX',
      propina:50
    }
  ];
  alumnos =[];
  displayedColumns: string[] = ['nombre', 'edad', 'ciudad', 'propina','nuevacolumna'];
  dataSource: MatTableDataSource<TableDaniel> = new MatTableDataSource();
  constructor(){

  }

  ngOnInit(){

   
   setTimeout(() => {
    this.data.map((valor:any)=>{
      valor.nuevacolumna=valor.edad + valor.propina;
    })
   });
   console.log(this.data);
    this.alumnos=this.data;
    this.dataSource = new MatTableDataSource(this.alumnos);
    setTimeout(() => {
      this.dataSource.sort = this.sort; 
    });
  }
  getTotalCost() {
    return this.alumnos.map(t => t.propina).reduce((acc, value) => acc + value, 0);
  }
  getTotalCostNuevaColumna(){
    return this.alumnos.map(t => t.nuevacolumna).reduce((acc, value) => acc + value, 0);
  }
}
