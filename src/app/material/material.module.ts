import { NgModule } from '@angular/core';
import { MatTableModule, MatSortModule  } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
@NgModule({
  declarations: [],
  imports: [
    MatTableModule,
    MatSortModule,
    BrowserAnimationsModule
  ],
  exports:[
    MatTableModule,
    MatSortModule,
    BrowserAnimationsModule
  ]
})
export class MaterialModule { }
